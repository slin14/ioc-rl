function cost = half_joint_half_task_objective(index1, index2)
% Purpose: compute the cost for the half_joint_half_task_objective
% Input: 2 time indices
% Prereq: run process_data.m so that the following are loaded
% - mdl, the RL model
% - POSITION - joint positions of the trajectory

% task space
% Apply the joint angles for 1st time index
mdl.position = POSITION(:,index1);
mdl.forwardPosition();

% Store the homogenous transform of the end effector (frame 13, gripper_tool_frame)
end_eff = mdl.getFrameByName('frame13'); 

% Get the position of the end effector
pos1 = end_eff.t(1:3, 4);

% Repeat the same process with 2nd time index
mdl.position = POSITION(:,index2);
mdl.forwardPosition();
end_eff = mdl.getFrameByName('frame13');
pos2 = end_eff.t(1:3, 4);

% compute the displacement in task space
displace = (pos1 - pos2);

% joint space
% Get the change in position (theta) of each joint at each time index
theta1 = POSITION(:,index1);
theta2 = POSITION(:,index2);

% Compute the cost for the half_joint_half_task_objective
cost = sum((theta2 - theta1).^2) * 0.5 + norm(displace.^2) * 0.5;