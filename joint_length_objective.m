function cost = joint_length_objective(index1, index2)
% Purpose: compute the cost for the joint_length_objective
% Input: 2 time indices
% Prereq: run process_data.m so that the following are loaded
% - POSITION - joint positions of the trajectory

% Get the change in position (theta) of each joint at each time index
theta1 = POSITION(:,index1);
theta2 = POSITION(:,index2);

% Return the cost for the joint_length_objective
cost = sum((theta2 - theta1).^2);