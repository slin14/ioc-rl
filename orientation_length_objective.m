function cost = orientation_length_objective(index1, index2)
% Purpose: compute the cost for the orientation_length_objective
% Input: 2 time indices
% Prereq: run process_data.m so that the following are loaded
% - mdl, the RL model
% - POSITION - joint positions of the trajectory

% Apply the joint angles for 1st time index
mdl.position = POSITION(index1);
mdl.forwardPosition();

% Store the homogenous transform of the end effector (frame 13, gripper_tool_frame)
end_eff = mdl.getFrameByName('frame13'); 

% Get the rotation of the end effector
rot1 = end_eff.t(1:3,1:3);

% Repeat the same process with 2nd time index
mdl.position = POSITION(index2);
mdl.forwardPosition();
end_eff = mdl.getFrameByName('frame13');
rot2 = end_eff.t(1:3,1:3);

% Compute the cost for the orientation_length_objective
cost = acos((trace(rot1 *  rot2') - 1) / 2);
