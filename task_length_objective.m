function cost = task_length_objective(index1, index2)
% Purpose: compute the cost for the task_length_objective
% Input: 2 time indices
% Prereq: run process_data.m so that the following are loaded
% - mdl, the RL model
% - POSITION - joint positions of the trajectory

% Apply the joint angles for 1st time index
mdl.position = POSITION(:,index1);
mdl.forwardPosition();

% Store the homogenous transform of the end effector (frame 13, gripper_tool_frame)
end_eff = mdl.getFrameByName('frame13'); 

% Get the position of the end effector
pos1 = end_eff.t(1:3, 4);

% Repeat the same process with 2nd time index
mdl.position = POSITION(:,index2);
mdl.forwardPosition();
end_eff = mdl.getFrameByName('frame13');
pos2 = end_eff.t(1:3, 4);

% Compute the cost for the task_length_objective
displace = (pos1 - pos2);
cost = norm(displace.^2);