function cost = manip_rot_objective(index)
% Purpose: compute the cost for the manip_rot_objective
% Input: a time index
% Prereq: run process_data.m so that the following are loaded
% - mdl, the RL model
% - POSITION - joint positions of the trajectory
% - VELOCITY - joint velocities of the trajectory
% - ACCELERATION - joint accelerations of the trajectory

% Apply the joint positions, velocties, and accelerations for the time index
mdl.position = POSITION(:,index);
mdl.velocity = VELOCITY(:,index);
mdl.acceleration = ACCELERATION(:,index);
mdl.forwardPosition();
mdl.forwardVelocity();
mdl.forwardAcceleration();
mdl.inverseDynamics();

% Grab the lower 3 rows of the Jacobian (rotational component)
J2 = mdl.J(4:6,:); 

% Compute the cost (mdl.J is the Jacobian)
cost = 1 / sqrt(det(J2 * J2'));